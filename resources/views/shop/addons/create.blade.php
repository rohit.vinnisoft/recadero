@extends('shop.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">@lang('inventory.addons.add_title')</h3>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form role="form" method="POST" action="{{ route('admin.addons.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <div style="display: flex;">
                        <label for="category">
                            @lang('inventory.addons.shop'):&ensp;
                        </label>
                        <p>{{Auth::user()->name}}</p>
                    </div>
                    <input type="hidden" value="{{Auth::user()->id}}" name="shop_id" />

                    @if ($errors->has('shop_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('shop_id') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">@lang('inventory.addons.name')</label>

                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus />

                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="row col-md-4">
                    <button type="submit" class="btn btn-primary btn-block">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
