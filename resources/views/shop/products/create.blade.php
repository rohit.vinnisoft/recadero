@extends('shop.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Create Product</h3>
        <div class="heading-elements">
            <a href="{{ route('shop.products.index') }}" class="btn btn-primary add-btn btn-darken-3">Back</a></li>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">

            <form role="form" method="POST" id="product-form" action="{{ route('shop.products.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <input type="hidden" name="shop" value="{{Auth::user()->id}}" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name</label>

                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
                            <span class="help-block text-danger" id="name-field"></span>

                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>

                        {{--<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">Description</label>

                            <textarea class="form-control" id="description-old" name="description" rows="3" required>{{ old('description') }}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>--}}

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category">Category</label>

                            <select class="form-control" id="category" name="category">

                                @foreach(Request::user()->categories as $Category)
                                    <option value="{{ $Category->id }}" >{{ $Category->name }}</option>

                                @endforeach

                            </select>
                            @if ($errors->has('category'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category') }}</strong>
                                </span>
                            @endif
                        </div>
                        {{--<div class="form-group{{ $errors->has('pure_veg') ? ' has-error' : '' }}">
                            <label for="parent_id">@lang('shop.create.pure_veg')</label>
                            <label class="radio-inline">
                                <input type="radio" value="non-veg" name="food_type">No
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="veg" name="food_type" checked >Yes
                            </label>

                            @if ($errors->has('food_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('food_type') }}</strong>
                                </span>
                            @endif
                        </div>--}}
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status">Status</label>

                            <select class="form-control" id="status" name="status">
                                <option value="enabled">Enabled</option>
                                <option value="disabled">Disabled</option>
                            </select>

                            @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>

                        {{--<div class="form-group{{ $errors->has('product_position') ? ' has-error' : '' }}">
                            <label for="status">@lang('inventory.product.product_position')</label>

                            <input type="number" class="form-control" id="product_position" name="product_position" required autofocus/>


                            @if ($errors->has('product_position'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('product_position') }}</strong>
                                </span>
                            @endif
                        </div>--}}

                        {{--<div class="form-group{{ $errors->has('featured') ? ' has-error' : '' }}">
                            <div class="">
                                <input type="checkbox" value="1" class="" id="featured" name="featured"/>
                                <label for="featured">@lang('inventory.product.featured')</label>
                            </div>
                            <label for="featured">@lang('inventory.product.featured_position')</label>
                            <input type="number" min="0" class="form-control" value="1" id="featured_position" name="featured_position"/>


                            @if ($errors->has('featured'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('featured') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('featured_image') ? ' has-error' : '' }}">
                            <label for="image">@lang('inventory.product.featured_image')</label>
                            <p>@lang('inventory.product.featured_image_note')</p>

                            <input type="file" accept="image/*" required name="featured_image" class="dropify form-control" id="featured_image" aria-describedby="fileHelp">

                            @if ($errors->has('featured_image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('featured_image') }}</strong>
                                </span>
                            @endif
                        </div>--}}
                        <div class="row">
                            <div class="col-md-3" style="padding-right: 0px;">
                                <label class="form-label">Weight</label>
                                <input type="text" class="form-control" name="weight[]" placeholder="In grams" value="{{ old('weight') }}" onkeypress="return onlyNumberKey(event)" required />
                            </div>
                            <div class="col-md-3" style="padding-right: 0px;">
                                <label class="form-label">Price</label>
                                <input type="text" class="form-control" name="prices[]" placeholder="Price" value="{{ old('price') }}" onkeypress="return onlyNumberKey(event)" required />
                            </div>
                            <div class="col-md-2" style="padding-right: 0px;">
                                <label class="form-label">QTY</label>
                                <input type="text" class="form-control" name="qty[]" placeholder="QTY" value="{{ old('qty') }}" min="0" onkeypress="return onlyNumberKey(event)" required />
                            </div>
                            <div class="col-md-4" style="padding-right: 0px;">
                                <div class="mt-2" style="padding-top: 5px;">
                                  <button type="button" class="btn btn-primary" onclick="addMore()" name="button"><strong>+</strong></button>
                                </div>
                            </div>
                        </div>
                        <div class="new-section"></div>

                    </div>
                    <div class="col-md-6">

                      {{--  <h4 class="m-t-0 header-title">
                            <b>Pricing</b>
                        </h4>
                        <hr>--}}

                        {{--<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price">Price</label>

                            <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

                            @if ($errors->has('price'))
                            <span class="help-block">
                                <strong>{{ $errors->first('price') }}</strong>
                            </span>
                            @endif
                        </div>--}}

                        <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}" style="display:none">
                            <label for="discount">Discount</label>

                            <input id="discount" type="text" class="form-control" name="discount" value="{{ old('discount', 0) }}" required autofocus>

                            @if ($errors->has('discount'))
                            <span class="help-block">
                                <strong>{{ $errors->first('discount') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('discount_type') ? ' has-error' : '' }}" style="display:none">
                            <label for="discount_type">Discount Type</label>

                            <select class="form-control" id="discount_type" name="discount_type">
                                <option value="percentage">Percentage</option>
                                <option value="amount">Amount</option>
                            </select>

                            @if ($errors->has('discount_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('discount_type') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description">Description</label>

                            <textarea class="form-control" id="description" name="description" rows="6" placeholder="Description" required>{{ old('description') }}</textarea>
                            <span class="help-block text-danger" id="description-field"></span>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image">Image</label>

                            <input type="file" accept="image/*" required name="avatar[]" class="dropify form-control" id="image" aria-describedby="fileHelp">
                            <span class="help-block text-danger" id="image-field"></span>

                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>

                        <!-- <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
                            <label for="currency">Currency</label>

                            <select class="form-control" id="currency" name="currency">
                                <option value="₹">₹ - Rupee</option>
                                <option value="$">$ - Dollars</option>
                                <option value="£">£ - Sterling Pounds</option>
                            </select>

                            @if ($errors->has('currency'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('currency') }}</strong>
                                </span>
                            @endif
                        </div> -->


                        {{--@if(Setting::get('PRODUCT_ADDONS')==1)
                        <div class="form-group{{ $errors->has('addons') ? ' has-error' : '' }}">
                            <label for="addons">@lang('inventory.product.addons')</label>
                            @forelse($Addons as $key=>$addon)
                            <p><input type="checkbox" name="addons[]" value="{{$addon->id}}">{{$addon->name}}</p>
                            <p>Price</p>
                            <p><input type="text" class="form-control" name="addons_price[]" value="0"></p>
                            @empty
                            @endforelse
                            @if ($errors->has('addons'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('addons') }}</strong>
                                </span>
                            @endif
                        </div>
                        @endif--}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 my-2">
                        <a href="{{ route('shop.products.index') }}" class="btn btn-warning mr-1">
                            <i class="ft-x"></i> Cancel
                        </a>
                        <button type="button" id="save-product" class="btn btn-primary">
                            <i class="fa fa-check-square-o"></i> Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/admin/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/dropify/dist/js/dropify.min.js') }}"></script>
<script type="text/javascript">
$('#save-product').on('click', function() {
    if ($('#name').val() == '') {
        $('#name-field').text('Name field is required.');
    } else {
        // if ($('#name').val().indexOf(' ') >= 0) {
        var name = $('#name').val();
        if (name.replaceAll(' ', '').length == 0) {
            $('#name-field').text('Whitespace not allowed.');
        } else {
            $('#name-field').text('');
        }
    }
    if ($('#description').val() == '') {
        $('#description-field').text('Description field is required.');
    } else {
        // if ($('#description').val().indexOf(' ') >= 0) {
        var description = $('#description').val();
        if (description.replaceAll(' ', '').length == 0) {
            $('#description-field').text('Whitespace not allowed.');
        } else {
            $('#description-field').text('');
        }
    }
    if ($('#image').val() == '') {
        $('#image-field').text('Image field is required.');
    } else {
        // if ($('#image').val().indexOf(' ') >= 0) {
        var image = $('#image').val();
        if (image.replaceAll(' ', '').length == 0) {
            $('#image-field').text('Whitespace not allowed.');
        } else {
            $('#image-field').text('');
        }
    }

    if ($('#name').val() != '' && $('#description').val() != '' && $('#image').val() != '') {
        $('#product-form').submit();
    }
});
function onlyNumberKey(evt) {
    // Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
$(document).ready(function() {
    setTimeout(function(){
        $('.alert-success').prop('hidden', true);
    }, 5000);
});
function addMore() {
    var _html = `<div class="row mt-1">
        <div class="col-md-3" style="padding-right: 0px;">
            <label class="form-label">Weight</label>
            <input type="text" class="form-control" name="weight[]" placeholder="In grams" value="{{ old('weight') }}" onkeypress="return onlyNumberKey(event)" required />
        </div>
        <div class="col-md-3" style="padding-right: 0px;">
            <label class="form-label">Price</label>
            <input type="text" class="form-control" name="prices[]" placeholder="Price" value="{{ old('price') }}" onkeypress="return onlyNumberKey(event)" required />
        </div>
        <div class="col-md-2" style="padding-right: 0px;">
            <label class="form-label">QTY</label>
            <input type="text" class="form-control" name="qty[]" placeholder="QTY" value="{{ old('qty') }}" min="0" onkeypress="return onlyNumberKey(event)" required />
        </div>
        <div class="col-md-4" style="padding-right: 0px;">
            <div class="mt-2" style="padding-top: 5px;">
              <button type="button" class="btn btn-primary" onclick="addMore()" name="button"><strong>+</strong></button>
              <button type="button" class="btn btn-danger" onclick="removeRow(this)" name="button"><strong>x</strong></button>
            </div>
        </div>
    </div>`;
    $('.new-section').append(_html);
}

function removeRow(element) {
    var test = element.parentElement.parentElement.parentElement;
    test.remove();
}

$('#categories').multiSelect({ selectableOptgroup: true });
$('.dropify').dropify();
</script>
@endsection
