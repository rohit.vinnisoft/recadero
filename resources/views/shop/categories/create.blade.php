@extends('shop.layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Create Category</h3>
        <div class="heading-elements">
            <a href="{{ route('shop.categories.index') }}" class="btn btn-primary add-btn btn-darken-3">Back</a></li>
        </div>
    </div>
    <div class="card-body collapse in">
        <div class="card-block">
            <form role="form" method="POST" action="{{ route('shop.categories.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Name</label>
                    <select class="form-control" name="name">
                        <option value="" selected disabled>Select Category Name</option>
                        @foreach($categoriesList as $categoryList)
                            <option value="{{ $categoryList->name }}">{{ $categoryList->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('name'))
                    <span class="help-block text-danger">
                        {{ $errors->first('name') }}
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description">Description</label>

                    <textarea class="form-control" id="description" name="description" rows="3">{{ old('description') }}</textarea>

                    @if ($errors->has('description'))
                        <span class="help-block text-danger">
                            {{ $errors->first('description') }}
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('shop_id') ? ' has-error' : '' }}">
                    <label for="parent_id">Shop: @if(Auth::user()->id) {{\App\Shop::find(Auth::user()->id)->name}} @endif</label>
                    <input name="shop_id" type="hidden" value="{{Auth::user()->id}}" />
                    @if ($errors->has('shop_id'))
                        <span class="help-block text-danger">
                            {{ $errors->first('shop_id') }}
                        </span>
                    @endif
                </div>
                 @if(Setting::get('SUB_CATEGORY',0))
                 <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                    <label for="parent_id">Parent</label>

                    <select class="form-control" id="parent_id" name="parent_id">
                        <option value="0">None</option>
                        @forelse($Categories as $Category)
                        <option value="{{ $Category->id }}">{{ $Category->name }}</option>
                        @empty
                        <option value="0">None</option>
                        @endforelse
                    </select>

                    @if ($errors->has('parent_id'))
                        <span class="help-block text-danger">
                            {{ $errors->first('parent_id') }}
                        </span>
                    @endif
                </div>
                @endif
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label for="status">Status</label>

                    <select class="form-control" id="status" name="status">
                        <option value="enabled">Enabled</option>
                        <option value="disabled">Disabled</option>
                    </select>

                    @if ($errors->has('status'))
                        <span class="help-block text-danger">
                            {{ $errors->first('status') }}
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                    <label for="status">@lang('inventory.category.position')</label>
                    <input type="number" class="form-control" value="" id="position" name="position" min="0" onkeyup="onlyNumbers(this)"/>
                        @if ($errors->has('position'))
                            <span class="help-block text-danger">
                                {{ $errors->first('position') }}
                            </span>
                        @endif
                </div>
                {{--<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label for="image">Image</label>

                    <input type="file" accept="image/*" name="image" class="dropify" id="image" aria-describedby="fileHelp">

                    @if ($errors->has('image'))
                        <span class="help-block text-danger">
                            {{ $errors->first('image') }}
                        </span>
                    @endif
                </div>--}}

                 <div class="col-xs-12 mb-2">
                    <a href="{{ route('shop.categories.index') }}" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </a>
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('scripts')
<script type="text/javascript">
function onlyNumbers(el)
{
    var text = $(el).val();
    $(el).val(text.replace('-', ''));
}
</script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/dropify/dist/js/dropify.min.js') }}"></script>
<script type="text/javascript">
    $('#categories').multiSelect({ selectableOptgroup: true });
    $('.dropify').dropify();
</script>
@endsection
