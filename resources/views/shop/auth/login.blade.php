@extends('admin.layouts.auth')

@section('content')
<div class="login bg-img" style="background-image: url({{asset('assets/img/store.jpg')}});">
        <div class="login-overlay"></div>
        <div class="login-content">

            <div class="login-content-inner">
                <div class="login-head">
                    <h1 class="">{{Setting::get('site_title')}}</h1>
                    <h3>Login to Your Account</h3>
                    @if(Session::has('login-error'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ Session::get('login-error') }}
                        </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    {{--@include('include.alerts')--}}
                </div>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/shop/login') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Email</label>
                         <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ old('email') }}" autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input id="password" type="password" class="form-control" placeholder="Password" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block text-danger">
                                {{ $errors->first('password') }}
                            </span><br>
                        @endif
                        <input type="checkbox" name="" value="" class="mt-1" id="show-password"> Show Password
                    </div>
                    <button class="btn btn-primary btn-block">Login</button>
                    <a href="{{ url('/shop/password/reset') }}" class="forgot-link">Forgot Password?</a>
                    <a href="{{url('/shop/register')}}" style="margin-left: 170px;" class="forgot-link">Signup</a>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
$(window).bind("pageshow", function() {
    var form = $('form');
    // let the browser natively reset defaults
    form[0].reset();
});
$('#show-password').on('click', function() {
    var password = document.getElementById("password");
    if (password.type === "password") {
        password.type = "text";
    } else {
        password.type = "password";
    }
});

$(document).ready(function() {
    setTimeout(function(){
        $('.alert-success').prop('hidden', true);
    }, 5000);
});
</script>
@endsection
