@extends('admin.layouts.auth')
@section('content')
<div class="login bg-img" style="background-image: url({{asset('assets/img/store.jpg')}});">
    <div class="login-overlay"></div>
    <div class="login-content">
        <div class="login-content-inner">
            <div class="login-head">
                <h1 class="">{{Setting::get('site_title')}}</h1>
                <h3>Forgot Password</h3>
                @if(Session::has('status'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        Password reset link sent on your register email id
                    </div>
                @endif
                {{--@include('include.alerts')--}}
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/shop/password/email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label">E-Mail Address</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                </div>
                <button class="btn btn-primary btn-block">Submit</button>
                <p class="log-txt">The verfication link will send to your Email Account</p>
                <div class="col-12" style="text-align: center; margin-top: 12px;">
                    <a href="{{url('/shop/login')}}" class="mt-3">Back to Login</a>
                </div>
            </form>
       </div>
    </div>
</div>
@endsection
