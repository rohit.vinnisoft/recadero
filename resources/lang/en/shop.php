<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop Language Lines
    |--------------------------------------------------------------------------
    */

    'index' => [
        'title' => 'Shop',
        'add_shop' => 'Add Shop',
        'sl_no' => 'Sr.No',
        'name' => 'Name',
        'image' => 'Image',
        'address' => 'Address',
        'contact_details' => 'Contact Details',
        'rating' => 'Rating',
        'action' => 'Action',
        'no_record_found' => 'No Shop Available Now'
    ],
    'create' => [
        'title' => 'Create Shop',
        'image' => 'Shop Image / Logo',
        'banner' => 'Shop Banner',
        'name' => 'Name',
        'email' => 'Email Address',
        'cuisine' => 'Cuisine',
        'phone' => 'Contact Details',
        'password' => 'Password',
        'confirm_password' => 'Confirm Password',
        'status' => 'Status',
        'everyday' => 'Shop Open Timing',
        'hours_opening' => 'Shop Opens',
        'hours_closing' => 'Shop Closes',
        'pure_veg' => 'Pure Veg',
        'Min_Amount' => 'Min Amount',
        'offer_percent' => 'Offer Percentage',
        'estimated_delivery_time' => 'Estimated delivery time',
        'description' => 'Description',
        'location' => 'Location',
        'address' => 'Address',
        'cancel' => 'Cancel',
        'save' => 'Save',
        'rating' => 'Rating',
        'fixed' => 'Fixed'
    ],
    'edit' => [
        'title' => 'Edit Shop',
    ],
    'created_success' => 'Shop :name Created Successfully',
    'updated_success' => 'Shop :name Updated Successfully',
    'removed_success' => 'Shop :name Removed Successfully'




];
