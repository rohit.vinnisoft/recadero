<?php
use App\Http\Controllers\SendPushNotification;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

/* -------- R Start -------- */
Route::post('/check-user', 'Auth\RegisterController@checkUser');
Route::post('/send-otp', 'Auth\RegisterController@sendOtp');
Route::post('/verify-otp', 'Auth\RegisterController@verifyOtp');
Route::post('/register', 'Auth\RegisterController@apiregister');
Route::post('/delete-user', 'Auth\RegisterController@delUser');
Route::post('/login', 'Auth\LoginController@userLogin');
/* --------- R End --------- */
Route::post('/social/login','SocialLoginController@loginWithSocial');
Route::post('/facebook/login','SocialLoginController@facebookViaAPI');
Route::post('/google/login','SocialLoginController@googleViaAPI');
Route::post('/otp', 'Auth\RegisterController@OTP');
Route::post('/forgot/password','Auth\ForgotPasswordController@forgot_password');
// Route::post('/oauth/token' , 'Auth\RegisterController@login');
Route::post('/reset/password', 'Auth\ResetPasswordController@reset_password');

Route::get('/cuisines', 'Resource\CuisineResource@index');
Route::get('/cuisines/{cuisine}', 'Resource\CuisineResource@show');
Route::get('/shops', 'Resource\ShopResource@index');
Route::get('/shops/{shop}', 'Resource\ShopResource@show');
Route::get('/shop-details/{shop_id}/{category_id?}', 'Resource\ShopResource@shopDetails');
// for demo
Route::post('/demoapp', 'ManageappController@details');
Route::get('/initsetup', 'ManageappController@setting');

Route::get('banner', 'AdminController@manageBanner');
// Categories List
Route::get('/categories', 'Resource\CategoryResource@index');
Route::get('/shop-categories/{shop_id}', 'Resource\CategoryResource@shopCategories');
Route::resource('products', 'Resource\ProductResource');
// Product List
Route::get('/categories/{category}', 'Resource\CategoryResource@show');
// Search
/* -------- R Start -------- */
Route::get('/get-categories', 'Resource\CategoryResource@getCategoryList');
Route::get('/category-shops/{category_id}', 'Resource\CategoryResource@categoryShops');
Route::get('/category/products/{shop_id}/{category_id}', 'Resource\CategoryResource@categoryProducts');
/* --------- R End --------- */
Route::get('search', 'UserResource\SearchResource@index');
/* -------- R Start -------- */
Route::post('search-category-shop', 'UserResource\SearchResource@searchCategoryShop');
/* --------- R End --------- */

Route::group(['middleware' => ['auth:api']], function() {

	Route::group(['prefix' => 'profile'], function() {
		Route::get('/', 'UserResource\ProfileController@index');
		Route::post('/', 'UserResource\ProfileController@update');

		Route::post('/password', 'UserResource\ProfileController@password');
	});
	Route::get('/logout', 'UserResource\ProfileController@logout');
	Route::resource('address', 'UserResource\AddressResource');
	Route::resource('cart', 'UserResource\CartResource');
	Route::resource('order', 'UserResource\OrderResource');
	Route::get('/ongoing/order', 'UserResource\OrderResource@orderprogress');
	Route::post('/reorder', 'UserResource\OrderResource@reorder');
	Route::resource('dispute', 'Resource\DisputeResource');
	Route::post('/rating', 'UserResource\OrderResource@rate_review');
	Route::resource('favorite', 'Resource\FavoriteResource');
	// wallet
	Route::get('wallet', 'UserResource\WalletResource@index');
	Route::get('wallet/promocode', 'UserResource\WalletResource@promocode');
	Route::post('wallet/promocode', 'UserResource\WalletResource@store');
	Route::get('/notification', 'UserResource\ProfileController@notifications');
	Route::get('/disputehelp','Resource\DisputeHelpResource@index');
	Route::get('/clear/cart','UserResource\CartResource@clearCart');
	Route::resource('card', 'Resource\CardResource');
	/* -------- R Start -------- */
	Route::get('get/cards', 'Resource\CardResource@getCards');
	Route::post('add/cards', 'Resource\CardResource@addCards');
	Route::delete('delete/card/{card_id}', 'Resource\CardResource@deleteCard');

	Route::post('send-otp-email-phone', 'UserResource\ProfileController@sendOtpEmailAndPhone');
	/* --------- R End --------- */
	Route::post('/payment' , 	'PaymentController@payment');
	Route::post('/add/money' , 	'PaymentController@add_money');
	Route::get('apply/promocode', 'UserResource\CartResource@apply_promocode');
});

Route::get('/pushnotification', function(){
		$data =  (new SendPushNotification)->sendPushToUser(106,'test test');
		dd($data);
});
