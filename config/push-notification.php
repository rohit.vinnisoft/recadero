<?php

return array(

    'IOSUser'     => array(
        'environment' =>env('IOS_USER_ENV', 'development'),
        'certificate' => app_path().'/apns/user/live_user.pem',
        'passPhrase'  => env('IOS_PUSH_PASS', 'Recadero123$'),
        'service'     =>'apns'
    ),
    'IOSProvider' => array(
        'environment' => env('IOS_PROVIDER_ENV', 'development'),
        'certificate' => app_path().'/apns/provider/live_provider.pem',
        'passPhrase'  => env('IOS_PROVIDER_PUSH_PASS', 'Recadero123$'),
        'service'     => 'apns'
    ),
    'IOSShop' => array(
        'environment' => env('IOS_SHOP_ENV', 'development'),
        'certificate' => app_path().'/apns/shop/RecaderoRestaurantDist.pem',
        'passPhrase'  => env('IOS_SHOP_PUSH_PASS', 'Recadero123$'),
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' =>env('ANDROID_ENV', 'development'),
        'apiKey'      =>'AIzaSyDDmyOZoYqRcAqFeQBP_NOaabPX3HIeXx0',
        'service'     =>'gcm'
    )

);
