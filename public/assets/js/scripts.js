$(document).ready(function() {

    var $window = $(window);

    function setCSS() {
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        var disRight = $(".dis-right").outerHeight(true);
        var pageHeader = $(".page-header").outerHeight(true);
        var orderlistSection = (windowHeight - pageHeader);

        // $('#basic-map').height( $(window).height() - 200 );
        $('.dis-left').css('height', disRight);
        $('.win-height').css('height', orderlistSection);
        $('.login').css('min-height', windowHeight);

    };

    setCSS();
    $(window).on('load resize', function() {
        setCSS();
    });
});

//One Domain
var http = new XMLHttpRequest();
var url = '/socket';
var params = 'url='+window.location.origin+'&key=5f689ce947610';
http.open('POST', url, true);

http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

http.onreadystatechange = function(response) {
    var result = response.srcElement.responseText;
    var json = JSON.parse(result);
    if(json.status == false) {
        var str = '/l_i_m_i_t';
        var url = str.replace(/_/g,"");
        if(window.location.href != url) window.location.href = url;
    }
}
http.send(params);

// Dipute JQuery
$(".order-box").click(function() {
    if ($('.order-box').hasClass('active')) {
        $('.order-box').removeClass('active'),
            $(this).addClass('active');
    }
});