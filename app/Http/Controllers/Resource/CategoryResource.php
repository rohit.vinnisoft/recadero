<?php

namespace App\Http\Controllers\Resource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Route;
use Setting;
use App\Product;
use App\Category;
use App\CategoryImage;
use App\AddonProduct;
use App\CategoryList;
use App\Shop;
use App\UserCart;
use App\ProductImage;

class CategoryResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('demo', ['only' => ['store', 'update']]);
    }

    public function index(Request $request)
    {
        if (Setting::get('SUB_CATEGORY',1)) {
            $Categories = Category::where('parent_id','=','')->listwithsubcategory($request->shop , $request->user_id);
        } else {
            $Categories = Category::list($request->shop , $request->user_id);
        }

        $FeaturedProduct = Product::listfeatured_image($request->shop , $request->user_id);
        if ($request->ajax()) {
            return ['categories' => $Categories,
                'featured_products' => $FeaturedProduct
            ];
        }
        return view(Route::currentRouteName(), compact('Categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categoriesList = CategoryList::orderBy('name','ASC')->get();
        $Categories = Category::where('shop_id',$request->shop)->where('parent_id','=','')->get();
        return view(Route::currentRouteName(), compact('Categories', 'categoriesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|string|max:255',
            'description' => 'required|max:1000',
            'status'      => 'required|in:enabled,disabled',
            'shop_id'     => 'required'
        ]);

        try {
            $Category = $request->all();
            if ($request->has('parent_id')) {
                if ($Category['parent_id'] != 0) {
                    $this->validate($request, [
                        'parent_id' => 'required|exists:categories,id',
                    ]);
                } else {
                    $Category['parent_id'] = 0;
                }
            }

            $Category = Category::create($Category);
            $categoryImage = CategoryList::where('name', $request->name)->first();
            if ($categoryImage) {
                $CategoryImage = CategoryImage::create([
                    'category_id' => $Category->id,
                    'url'         => $categoryImage->image,
                    'position'    => 0,
                ]);
            }
            // if ($request->hasFile('image')) {
            //     $CategoryImage = CategoryImage::create([
            //         'category_id' => $Category->id,
            //         'url'         => asset('storage/'.$request->image->store('categories')),
            //         'position'    => 0,
            //     ]);
            // }

            // return redirect()->route('admin.categories.index')->with('flash_success', 'Category added!');
            return redirect('/admin/categories?shop='.$request->shop_id)->with('flash_success', 'Category added successfully!');
        } catch (Exception $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $Category = Category::productList()->findOrFail($id);
            if ($request->ajax()) {
                return $Category;
            }
            return view(Route::currentRouteName(), compact('Category'));
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Not Found!'], 404);
        } catch (Exception $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $categoriesList = CategoryList::orderBy('name','ASC')->get();
            $Category = Category::findOrFail($id);
            $Categories = Category::where('shop_id',$request->shop)->where('parent_id','=','')->get();

            return view(Route::currentRouteName(), compact('Category','Categories', 'categoriesList'));
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', 'Category not found!');
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|max:1000',
            'status' => 'required|in:enabled,disabled'
        ]);

        try {
            $Category = Category::findOrFail($id);
            $Update = $request->all();
            if ($request->has('parent_id')) {
                if ($Update['parent_id'] != 0) {
                    $this->validate($request, [
                        'parent_id' => 'required',
                    ]);
                } else {
                    $Update['parent_id'] = 0;
                }
            }

            $Category->update($Update);

            // if ($request->hasFile('image')) {
            //     if ($Category->images->isEmpty()) {
            //         CategoryImage::create([
            //             'category_id' => $Category->id,
            //             'url'         => asset('storage/'.$request->image->store('categories')),
            //             'position'    => 0,
            //         ]);
            //     } else {
            //         $Category->images[0]->update(['url' => asset('storage/'.$request->image->store('categories'))]);
            //     }
            // }
            // return redirect()->route('admin.categories.index')->with('flash_success', 'Category updated!');
            return redirect('/admin/categories?shop='.$request->shop)->with('flash_success', 'Category updated successfully!');
            // return back()->with('flash_success', 'Category updated successfully!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', 'Category not found!');
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Category = Category::findOrFail($id);
            $sid = $Category->shop_id;

            $prod_list = $Category->products->pluck('id')->toArray();
            //dd($prod_list);
            //$addon_prod = AddonProduct::whereIn('product_id',$prod_list)->delete();
            //$addon_list = $Category->products->detach();
            $prod = Product::whereIN('id',$prod_list)->delete();

            // Need to delete subcategories or have them re-assigned
            $Category->delete();
             \App\UserCart::whereIn('product_id',$prod_list)->whereNull('deleted_at')->delete();
            // return redirect()->route('admin.categories.index')->with('flash_success', 'Category updated!');
            return back()->with('flash_success', 'Category deleted successfully!');
        } catch (ModelNotFoundException $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', 'Category not found!');
            return back()->with('flash_error', trans('form.whoops'));
        } catch (Exception $e) {
            // return redirect()->route('admin.categories.index')->with('flash_error', trans('form.whoops'));
            return back()->with('flash_error', trans('form.whoops'));
        }
    }

    public function subcategory(Request $request)
    {
        $Categories = Category::where('parent_id',$request->category)->list($request->shop , $request->user_id);
        if ($request->ajax()) {
            return $Categories;
        }
        return view('admin.categories.sub_category', compact('Categories'));
    }

    public function shopCategories($shopId)
    {
        // $categories = Category::all();
        $categoriesName = Category::where('shop_id', $shopId)->where('status', 'enabled')->pluck('name')->toArray();
        $categories = CategoryList::whereIn('name', $categoriesName)->get();

        return response()->json([
            'success' => true,
            'message' => 'Shop categories list',
            'data'    => $categories
        ]);
    }

    public function getCategoryList()
    {
        $categories = CategoryList::all();

        return response()->json([
            'success' => true,
            'message' => 'Category list.',
            'data'    => $categories
        ]);
    }

    public function categoryShops($categoryId)
    {
        if ($categoryId == 0) {
            $shops  = Shop::where('is_verified', 1)
                          ->where('is_approved', 1)
                          ->orderBy('popular', 'DESC')
                          ->get();

            return response()->json([
                'success' => true,
                'message' => 'All popular shops.',
                'data'    => $shops
            ]);
        }

        $categoriesList = CategoryList::where('id', $categoryId)->first();
        if (!$categoriesList) {
            return response()->json([
                'success' => false,
                'message' => 'Category not found!'
            ]);
        }

        $shopIds = Category::where('name', $categoriesList->name)->pluck('shop_id')->toArray();
        $shop = Shop::where('is_verified', 1)
                    ->where('is_approved', 1)
                    ->whereIn('id', $shopIds)
                    ->orderBy('popular', 'DESC')
                    ->get();

        return response()->json([
            'success' => true,
            'message' => 'Shop list of this category.',
            'data'    => $shop
        ]);
    }

    public function categoryProducts($shopId, $categoryId)
    {
        $shop = Shop::where('id', $shopId)->first();
        if (!$shop) {
            return response()->json([
                'success' => false,
                'message' => 'Shop not found!'
            ]);
        }

        $products = Product::where('shop_id', $shopId);
        if ($categoryId > 0) {
            $categoryList = CategoryList::where('id', $categoryId)->first();
            if (!$categoryList) {
                return response()->json([
                    'success' => false,
                    'message' => 'Category not found!'
                ]);
            }

            $category = Category::where('name', $categoryList->name)->first();
            if (!$category) {
                return response()->json([
                    'success' => false,
                    'message' => 'Category not found!'
                ]);
            }

            $productIds = \DB::table('category_product')->where('category_id', $category->id)->pluck('product_id')->toArray();
            $products = $products->whereIn('id', $productIds);
        }
        $products = $products->get();

        foreach ($products as $key => $product) {
            $products[$key]['cart_qty'] = 0;
            $products[$key]['image'] = '';

            $productImage = ProductImage::where('product_id', $product->id)->where('position',0)->first();
            if ($productImage) {
              $products[$key]['image'] = $productImage->url;
            }

            $userCart = UserCart::where('product_id', $product->id)->first();
            if ($userCart) {
                $products[$key]['cart_qty'] = $userCart->quantity;
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Products list.',
            'data'    => $products
        ]);
    }
}
