<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\CommonController;
use Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Twilio;
use Session;
use App\Http\Controllers\SocialLoginController;
use App\UserOtp;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'phone.unique' => 'You are already Registered',
            'phone.regex' => 'you have to add + in your phone number'
        ];

        // if(isset($data['login_by'])){
        if (isset($data['login_by']) && ($data['login_by'] == 'facebook' || $data['login_by'] == 'google' )) {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'phone' => 'required|unique:users|min:6|regex:/^[+][0-9]+$/',
                'accessToken'=>'required',
                'login_by' => 'required|in:manual,facebook,google'
            ], $messages);
        } else {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'phone' => ['required','unique:users','min:6','regex:/^[+][0-9]+$/'],
                'password' => 'required|string|min:6|confirmed',
            ], $messages);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $social_data = [];
        if (isset($data['login_by'])) {
            if ($data['login_by'] == 'facebook' || $data['login_by'] == 'google' ) {
                $social_data = (new SocialLoginController)->getSocialId($data);
            }
        }
        $User = User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'phone'    => $data['phone'],
            'password' => isset($data['password'])?bcrypt($data['password']):bcrypt('123456'),
            'login_by' => isset($data['login_by'])?$data['login_by']:'manual',
            'social_unique_id' => isset($data['accessToken'])?@$social_data->id:''
        ]);

        /*if (isset($data['login_by'])) {
            $userToken = $User->createToken('socialLogin');
            return response()->json([
                "status"       => true,
                "token_type"   => "Bearer",
                "access_token" => $userToken->accessToken
            ]);
        } else {*/
        return $User;
        //}
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('user.auth.register');
    }

    public function sendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        try {
            $data['phone']    = $request->phone;
            $newotp           = rand(1000,9999);
            $data['otp']      = $newotp;
            $msg_data         = send_sms($data);
            if ($msg_data) {
                $success = false;
                $msg = $msg_data;
            } else {
                $success = true;
                $msg = 'OTP sent to your number. Please verify your number.';
            }

            return response()->json([
                'success' => $success,
                'message' => $msg,
                'data'    => $data
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'error'   => trans('form.whoops')
            ], 500);
        }
    }

    public function sendOtpOld(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        try {
            $user = User::where('phone', $request->phone)->first();
            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'User not found!'
                ]);
            }

            $data['phone']    = $request->phone;
            $newotp           = rand(1000,9999);
            $data['otp']      = $newotp;
            $msg_data         = send_sms($data);
            if ($msg_data) {
                $success = false;
                $msg = $msg_data;
            } else {
                $success = true;
                $msg = 'OTP sent to your number. Please verify your number.';
            }

            $userOtp = UserOtp::where('user_id', $user->id)->first();
            if ($userOtp) {
                $userOtp->update([
                    'otp' => $newotp
                ]);
            } else {
                UserOtp::create([
                    'user_id' => $user->id,
                    'otp'     => $newotp,
                ]);
            }

            return response()->json([
                'success' => $success,
                'message' => $msg,
                'data'    => $user
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'error'   => trans('form.whoops')
            ], 500);
        }
    }

    public function verifyOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'otp'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        try {
            $user = User::where('phone', $request->phone)->first();
            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'User not found!'
                ]);
            }

            $userOtp = UserOtp::where('user_id', $user->id)->where('otp', $request->otp)->first();
            if (!$userOtp) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid OTP. Please enter a valid OTP.'
                ]);
            } else {
                $userOtp->delete();

                $this->guard()->login($user);
                $user->token = $user->createToken('MyApp')->accessToken;

                return response()->json([
                    'success' => true,
                    'message' => 'User login successfully.',
                    'data'    => $user
                ]);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error'   => trans('form.whoops')
            ], 500);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apiregister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        // $this->validator($request->all())->validate();
        // event(new Registered($user = $this->create($request->all())));

        try {
            // $userEmail = User::where('email', $request->email)->first();
            // if ($userEmail) {
            //     $userOtp = UserOtp::where('user_id', $user->id)->first();
            //     if ($userOtp) {
            //         return response()->where([
            //             'success' => false,
            //             'message' => 'The email has already been taken.'
            //         ]);
            //     } else {
            //         $userEmail->delete();
            //     }
            // }

            /* ----- New code Start ------ */
            // $user = User::where('phone', $request->phone)->first();
            // if ($user) {
            //     $userOtp = UserOtp::where('user_id', $user->id)->first();
            //
            //     if (!$userOtp) {
            //         return response()->json([
            //           'success' => false,
            //           'message' => 'This phone number is already registered.'
            //         ]);
            //     }
            //
            //     $user->update([
            //         'name'     => $request->name,
            //         'email'    => $request->email,
            //         'phone'    => $request->phone,
            //         'password' => bcrypt('123456'),
            //         'login_by' => 'manual',
            //         'social_unique_id' => NULL
            //     ]);
            // } else {
                $user = User::create([
                    'name'     => $request->name,
                    'email'    => $request->email,
                    'phone'    => $request->phone,
                    'password' => bcrypt('123456'),
                    'login_by' => 'manual',
                    'social_unique_id' => NULL
                ]);
            // }

            // $data['phone']    = $request->phone;
            // $newotp           = rand(1000,9999);
            // $data['otp']      = $newotp;
            // $msg_data         = send_sms($data);
            // if ($msg_data) {
            //     $user['msg_data'] = $msg_data;
            // } else {
            //     $user['msg_data'] = 'OTP sent to your number. Please verify your number.';
            // }
            //
            // $userOtp = UserOtp::where('user_id', $user->id)->first();
            // if ($userOtp) {
            //     $userOtp->update([
            //         'otp' => $newotp
            //     ]);
            // } else {
            //     UserOtp::create([
            //         'user_id' => $user->id,
            //         'otp'     => $newotp,
            //     ]);
            // }
            $this->guard()->login($user);
            $user->token = $user->createToken('MyApp')->accessToken;

            return response()->json([
                'success' => true,
                'message' => 'User registered successfully!',
                'data'    => $user,
            ]);
            /* ----- New code End ------ */

            // return response()->json([
            //     'success' => true,
            //     'message' => 'OTP Sent',
            //     'data'    => $msg_data
            // ]);

        } catch (\Exception $e) {
            return response()->json([
                'error'   => trans('form.whoops')
            ], 500);
        }
    }

    /**
     * Handle a OTP request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function OTP(Request $request)
    {
        $messages = [
            'phone.unique' => 'You are already Registered',
        ];
        if ($request->has('login_by')) {
            $this->validate($request, [
                'phone' => 'required|unique:users|min:6',
                'login_by' => 'required',
                'accessToken' => 'required'
            ],$messages);
        } else {
            $this->validate($request, [
                'phone' => 'required|unique:users|min:6'
            ],$messages);

        }
        try {
            $data = $request->all();
            if ($request->has('login_by')) {
                $social_data = (new SocialLoginController)->checkSocialLogin($request);
                //dd($social_data);
                if ($social_data) {
                    return response()->json([
                    'error' => trans('form.socialuser_exist'),
                ], 422);
                }
            } elseif (User::where('phone', $data['phone'])->first()) {
                return response()->json([
                    'error' => trans('form.mobile_exist'),
                ], 422);
            }
            $newotp      = rand(1000,9999);
            $data['otp'] = $newotp;
            $msg_data    = send_sms($data);

            //if($msg_data == null){
                return response()->json([
                    'message' => 'OTP Sent',
                    'otp' => $newotp
                ]);
            //}
            return response()->json(['error' => $msg_data], 422);

        } catch (Exception $e) {
            return response()->json(['error' => trans('form.whoops')], 500);
        }
    }

    public function checkUser(Request $request)
    {
        if (!$request->has('phone_number') && !$request->has('email')) {
            return response()->json([
                'success' => false,
                'message' => 'User not found!'
            ]);
        }

        $user = new User;
        if ($request->has('phone_number')) {
            $user = $user->where('phone', $request->phone_number);
        }

        if ($request->has('email')) {
            $user = $user->where('email', $request->email);
        }

        $user = $user->first();

        if ($user) {
            $msg  = 'User already exist!';
            $data = false;
        } else {
            $msg  = 'User not found!';
            $data = true;
        }

        return response()->json([
            'success' => true,
            'message' => $msg,
            'data'    => $data
        ]);
    }

    public function delUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        $user = User::where('phone', $request->phone)->first();
        if (!$user) {
            return response()->json([
                'success' => true,
                'message' => 'User not found!'
            ]);
        }
        $user->delete();

        return response()->json([
            'success' => true,
            'message' => 'User deleted successfully!'
        ]);
    }
}
