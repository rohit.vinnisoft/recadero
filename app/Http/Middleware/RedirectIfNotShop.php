<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Shop;

class RedirectIfNotShop
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'shop')
	{
	    if (!Auth::guard($guard)->check()) {
	        return redirect('shop/login');
	    }
			$verifyEmail = Shop::where('email', Auth::user()->email)->where('is_verified', 1)->first();
			if (!$verifyEmail) {
					Auth::logout();
					return redirect('shop/login')->with('login-error', 'Your email is not verified. Please verify your email before login.');
			}

			$shop = Shop::where('email', Auth::user()->email)->where('is_approved', 1)->first();
			if (!$shop) {
					Auth::logout();
					return redirect('shop/login')->with('login-error', 'Your account is not approved by admin. Please wait for approval.');
			}

	    return $next($request);
	}
}
