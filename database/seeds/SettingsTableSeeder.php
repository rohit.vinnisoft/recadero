<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            [
                'key' => 'site_title',
                'value' => 'Recadero'
            ],
            [
                'key' => 'site_logo',
                'value' => asset('site-logo.png'),
            ],
            [
                'key' => 'site_favicon',
                'value' => asset('site-logo.png'),
            ],
            [
                'key' => 'site_copyright',
                'value' => '&copy; '.date('Y').' Recadero'
            ],
            [
                'key' => 'delivery_charge',
                'value' => 20
            ],
            [
                'key' => 'shop_response_time',
                'value' => 5
            ],
            [
                'key' => 'currency',
                'value' => '$'
            ],
            [
                'key' => 'currency_code',
                'value' => 'USD'
            ],
            [
                'key' => 'search_distance',
                'value' => 10
            ],
            [
                'key' => 'tax',
                'value' => 10
            ],
            [
                'key' => 'payment_mode',
                'value' => 'CASH'
            ],
            [
                'key' => 'manual_assign',
                'value' => '0'
            ],
            [
                'key' => 'transporter_response_time',
                'value' => '30'
            ],
            [
                'key' => 'GOOGLE_API_KEY',
                'value' => 'AIzaSyC9oerN9FwkzJeQZmAgksVJ7A5jUsrUWxU'
            ],
            [
                'key' => 'android_api_key',
                'value' => '57446546568768hdfgdgfg'
            ],
            [
                'key' => 'ios_api_key',
                'value' => '57446546568768hdfgdgfg'
            ],
            [
                'key' => 'TWILIO_SID',
                'value' => ''
            ],
            [
                'key' => 'TWILIO_TOKEN',
                'value' => ''
            ],
            [
                'key' => 'TWILIO_FROM',
                'value' => ''
            ],
            [
                'key' => 'PUBNUB_PUB_KEY',
                'value' => ''
            ],
            [
                'key' => 'PUBNUB_SUB_KEY',
                'value' => ''
            ],
            [
                'key' => 'stripe_charge',
                'value' => '0'
            ],
            [
                'key' => 'stripe_publishable_key',
                'value' => 'pk_test_wdfwewqedwe232q323323232'
            ],
            [
                'key' => 'stripe_secret_key',
                'value' => 'sk_test_sdfsdf3456b4564b565645b6'
            ],
            [
                'key' => 'FB_CLIENT_ID',
                'value' => '29098481808487578'
            ],
            [
                'key' => 'FB_CLIENT_SECRET',
                'value' => '1f52cb4378e623bb819cd8469e548784'
            ],
            [
                'key' => 'FB_REDIRECT',
                'value' => url('/')
            ],
            [
                'key' => 'GOOGLE_CLIENT_ID',
                'value' => '4548756700052-rts0nr0a49dutr831oin38aj7mjju2ua.apps.googleusercontent.com'
            ],
            [
                'key' => 'GOOGLE_CLIENT_SECRET',
                'value' => 'FL0YR5dw9RuV6OdI88745asasw'
            ],
            [
                'key' => 'GOOGLE_REDIRECT',
                'value' => url('/')
            ],
            [
                'key' => 'ANDROID_ENV',
                'value' => 'development'
            ],
            [
                'key' => 'ANDROID_PUSH_KEY',
                'value' => 'AIzaSyBzvWOYuhyRuNXBKp6vxBBRMizNJj_1dFtg2F'
            ],
            [
                'key' => 'IOS_USER_ENV',
                'value' => 'development'
            ],
            [
                'key' => 'IOS_PROVIDER_ENV',
                'value' => 'development'
            ],
            [
                'key' => 'SUB_CATEGORY',
                'value' => '0'
            ],
            [
                'key' => 'SCHEDULE_ORDER',
                'value' => '0'
            ],
            [
                'key' => 'client_id',
                'value' => '2'
            ],
            [
                'key' => 'client_secret',
                'value' => '0'
            ],
            [
                'key' => 'PRODUCT_ADDONS',
                'value' => '1'
            ],
            [
                'key' => 'BRAINTREE_ENV',
                'value' => 'sandbox'
            ],
            [
                'key' => 'BRAINTREE_MERCHANT_ID',
                'value' => 'twbd779hfc859jxqJKui'
            ],
            [
                'key' => 'BRAINTREE_PUBLIC_KEY',
                'value' => '7bn6hystx7vs2g8rwdEE'
            ],
            [
                'key' => 'BRAINTREE_PRIVATE_KEY',
                'value' => '721e48cc74fdf2dfaacc6c3410487Gtrs'
            ],
             [
                'key' => 'RIPPLE_KEY',
                'value' => 'rEsaDShsYPmMZopoG3nNjutWJ7Poijh4'
            ],
            [
                'key' => 'RIPPLE_BARCODE',
                'value' => url('/images/ripple.png')
            ],
            [
                'key' => 'ETHER_ADMIN_KEY',
                'value' => '0x16abb22fd68c25286d72e77226ddaad87787SDsd'
            ],
            [
                'key' => 'ETHER_KEY',
                'value' => 'R92FW87ER1QZIDYX1UHTVBY625T8GTADER'
            ],
            [
                'key' => 'ETHER_BARCODE',
                'value' => url('/images/ether.jpeg')
            ],
            [
                'key' => 'CLIENT_AUTHORIZATION',
                'value' => 'sandbox_v5r97hvk_twbd779hfc847ftds'
            ],
            [
                'key' => 'SOCIAL_FACEBOOK_LINK',
                'value' => 'http://facebook.com'
            ],
            [
                'key' => 'SOCIAL_TWITTER_LINK',
                'value' => 'http://twitter.com'
            ],
            [
                'key' => 'SOCIAL_G-PLUS_LINK',
                'value' => 'http://google.com'
            ],
            [
                'key' => 'SOCIAL_INSTAGRAM_LINK',
                'value' => 'http://instagram.com'
            ],
            [
                'key' => 'SOCIAL_PINTEREST_LINK',
                'value' => 'http://pinterest.com'
            ],
            [
                'key' => 'SOCIAL_VIMEO_LINK',
                'value' => 'http://vimeo.com'
            ],
            [
                'key' => 'SOCIAL_YOUTUBE_LINK',
                'value' => 'http://youtube.com'
            ],
            [
                'key' => 'COMMISION_OVER_FOOD',
                'value' => '5'
            ],
            [
                'key' => 'COMMISION_OVER_DELIVERY_FEE',
                'value' => '10'
            ],
            [
                'key' => 'IOS_APP_LINK',
                'value' => 'https://itunes.apple.com/in/app/order-around/id135858ss18710?mt=8'
            ],
            [
                'key' => 'ANDROID_APP_LINK',
                'value' => 'https://play.google.com/store/apps/details?id=com.orderaround-old.user'
            ],
            [
                'key' => 'default_lang',
                'value' => 'en'
            ],
            [
                'key' => 'DEMO_MODE',
                'value' => '1'
            ],

        ]);
    }
}
