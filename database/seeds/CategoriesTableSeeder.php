<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('categories')->truncate();
        DB::table('categories')->insert([
            [
                'name' => 'Bread/Bakery',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Dairy',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Frozen Foods',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Meat',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Beverages',
                'shop_id' => 1,
                'description' => 'special'
            ],
            [
                'name' => 'Frozen Foods ',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Bread/Bakery',
                'shop_id' => 2,
                'description' => 'special'
            ],
             [
                'name' => 'Produce',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Cleaners',
                'shop_id' => 2,
                'description' => 'special'
            ],
            [
                'name' => 'Paper Goods',
                'shop_id' => 3,
                'description' => 'special'
            ],
             [
                'name' => 'Canned/Jarred Goods',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Personal Care',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Bread/Bakery',
                'shop_id' => 3,
                'description' => 'special'
            ],
            [
                'name' => 'Canned/Jarred Goods',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Personal Care',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Bread/Bakery',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Beverages',
                'shop_id' => 4,
                'description' => 'special'
            ],
            [
                'name' => 'Paper Goods',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Personal Care',
                'shop_id' => 5,
                'description' => 'special'
            ],
            [
                'name' => 'Canned/Jarred Goods',
                'shop_id' => 6,
                'description' => 'special'
            ],
            [
                'name' => 'Personal Care',
                'shop_id' => 6,
                'description' => 'special'
            ]
        ]);
    }
}
