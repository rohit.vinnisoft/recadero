<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_addresses')->truncate();
        DB::table('user_addresses')->insert([
            [
                'user_id'     => 1,
                'building'    => 'FWH3+266',
                'street'      => '1231',
                'city'        => 'Higuerote',
                'state'       => 'Miranda',
                'country'     => 'Venezuela',
                'pincode'     => '1231',
                'landmark'    => NULL,
                'map_address' => 'FWH3+266, Higuerote 1231, Miranda, Venezuela',
                'latitude'    => '10.47645600',
                'longitude'   => '-66.09784700',
                'type'        => 'home',
            ],
            [
                'user_id'     => 1,
                'building'    => 'FV6H+5F',
                'street'      => '1231',
                'city'        => 'Higuerote',
                'state'       => 'Miranda',
                'country'     => 'Venezuela',
                'pincode'     => '1231',
                'landmark'    => NULL,
                'map_address' => 'FV6H+5F Higuerote, Miranda, Venezuela',
                'latitude'    => '10.4604375',
                'longitude'   => '-66.1235065',
                'type'        => 'home',
            ],
            [
                'user_id'     => 2,
                'building'    => 'FWH3+266',
                'street'      => '1231',
                'city'        => 'Higuerote',
                'state'       => 'Miranda',
                'country'     => 'Venezuela',
                'pincode'     => '1231',
                'landmark'    => NULL,
                'map_address' => 'GV2P+29 Higuerote, Miranda, Venezuela',
                'latitude'    => '10.5000625',
                'longitude'   => '-66.1162565',
                'type'        => 'home',
            ],
            [
                'user_id'     => 3,
                'building'    => 'FV6R+FF7',
                'street'      => '1231',
                'city'        => 'Higuerote',
                'state'       => 'Miranda',
                'country'     => 'Venezuela',
                'pincode'     => '1231',
                'landmark'    => NULL,
                'map_address' => 'FV6R+FF7, Higuerote 1231, Miranda, Venezuela',
                'latitude'    => '10.4611625',
                'longitude'   => '-66.1110221',
                'type'        => 'office',
            ],
            [
                'user_id'     => 3,
                'building'    => 'FWH3+266',
                'street'      => '1231',
                'city'        => 'Higuerote',
                'state'       => 'Miranda',
                'country'     => 'Venezuela',
                'pincode'     => '1231',
                'landmark'    => NULL,
                'map_address' => 'FV2J+PF Higuerote, Miranda, Venezuela',
                'latitude'    => '10.4518125',
                'longitude'   => '-66.1210065',
                'type'        => 'office',
            ],
        ]);
    }
}
