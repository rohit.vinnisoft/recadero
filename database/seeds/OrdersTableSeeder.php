<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->truncate();
        DB::table('orders')->insert([
            [
                'invoice_id'             => 1,
                'user_id'                => 1,
                'shift_id'               => NULL,
                'user_address_id'        => 1,
                'shop_id'                => 1,
                'transporter_id'         => 111,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-21 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'ORDERED',
            ],
            [
                'invoice_id'             => 2,
                'user_id'                => 2,
                'shift_id'               => NULL,
                'user_address_id'        => 3,
                'shop_id'                => 1,
                'transporter_id'         => 112,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-23 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'ORDERED',
            ],
            [
                'invoice_id'             => 3,
                'user_id'                => 3,
                'shift_id'               => NULL,
                'user_address_id'        => 2,
                'shop_id'                => 1,
                'transporter_id'         => 113,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-21 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'PROCESSING',
            ],
            [
                'invoice_id'             => 4,
                'user_id'                => 4,
                'shift_id'               => NULL,
                'user_address_id'        => 4,
                'shop_id'                => 1,
                'transporter_id'         => 114,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-21 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'ASSIGNED',
            ],
            [
                'invoice_id'             => 5,
                'user_id'                => 5,
                'shift_id'               => NULL,
                'user_address_id'        => 2,
                'shop_id'                => 1,
                'transporter_id'         => 115,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-21 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'REACHED',
            ],
            [
                'invoice_id'             => 6,
                'user_id'                => 6,
                'shift_id'               => NULL,
                'user_address_id'        => 5,
                'shop_id'                => 1,
                'transporter_id'         => 116,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-21 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'PICKEDUP',
            ],
            [
                'invoice_id'             => 7,
                'user_id'                => 7,
                'shift_id'               => NULL,
                'user_address_id'        => 3,
                'shop_id'                => 1,
                'transporter_id'         => 117,
                'transporter_vehicle_id' => NULL,
                'reason'                 => NULL,
                'note'                   => NULL,
                'route_key'              => '',
                'dispute'                => 'NODISPUTE',
                'delivery_date'          => '2022-02-21 00:00:00',
                'order_otp'              => rand(1000, 9999),
                'order_ready_time'       => 0,
                'order_ready_status'     => 0,
                'status'                 => 'CANCELLED',
            ],
        ]);
    }
}
